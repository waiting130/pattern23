package com.chenyu;

/**
 * 领导者
 *
 * @author yu_chen
 * @create 2018-01-25 10:36
 **/
public abstract class Leader {

    protected String name;
    protected Leader nextLeader;

    public Leader(String name) {
        this.name = name;
    }

    /**
     * 设置下一级的leader
     *
     * @param nextLeader
     */
    public void setNextLeader(Leader nextLeader) {
        this.nextLeader = nextLeader;
    }

    /**
     * 处理请假的请求
     *
     * @param leaveRequest
     */
    public abstract void handlerRequest(LeaveRequest leaveRequest);
}
