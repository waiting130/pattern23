package com.chenyu;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yu_chen
 * @create 2018-01-25 10:45
 **/
public class Client {

    public static void main(String[] args) {

        //可以从数据库中按顺序查询出来
        Leader a = new Director("张三");
        Leader b = new Manager("李四");
        Leader c = new GeneralManager("王五");

        List<Leader> leaders = new ArrayList<>();
        leaders.add(a);
        leaders.add(b);
        leaders.add(c);

        for (int i = 0; i < leaders.size(); i++) {
            Leader leader = leaders.get(i);
            if (i < leaders.size() - 2) {
                leader.setNextLeader(leaders.get(i + 1));
            }
        }

        LeaveRequest leaveRequest = new LeaveRequest("yuchen", 5, "回家");

        a.handlerRequest(leaveRequest);
    }
}
