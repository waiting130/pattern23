package com.chenyu;

/**
 * 主管
 *
 * @author yu_chen
 * @create 2018-01-25 10:38
 **/
public class Director extends Leader {

    public Director(String name) {
        super(name);
    }

    @Override
    public void handlerRequest(LeaveRequest leaveRequest) {
        if (leaveRequest.getLeaveDays() < 3) {
            System.out.println("员工:" + leaveRequest.getEmpName() + "==请假天数:" + leaveRequest.getLeaveDays() + "==请假原因:" + leaveRequest.getReason());
            System.out.println("主管:" + this.name + "审批通过!");
        } else {
            if (this.nextLeader != null) {
                this.nextLeader.handlerRequest(leaveRequest);
            }
        }
    }
}
