package com.chenyu;

/**
 * 请假的请求对象
 *
 * @author yu_chen
 * @create 2018-01-25 10:34
 **/
public class LeaveRequest {

    /**
     * 员工姓名
     */
    private String empName;
    /**
     * 请假天数
     */
    private int leaveDays;
    /**
     * 请假原因
     */
    private String reason;


    public LeaveRequest(String empName, int leaveDays, String reason) {
        this.empName = empName;
        this.leaveDays = leaveDays;
        this.reason = reason;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public int getLeaveDays() {
        return leaveDays;
    }

    public void setLeaveDays(int leaveDays) {
        this.leaveDays = leaveDays;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
