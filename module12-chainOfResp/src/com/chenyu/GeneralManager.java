package com.chenyu;

/**
 * 经理
 *
 * @author yu_chen
 * @create 2018-01-25 10:38
 **/
public class GeneralManager extends Leader {

    public GeneralManager(String name) {
        super(name);
    }

    @Override
    public void handlerRequest(LeaveRequest leaveRequest) {
        if (leaveRequest.getLeaveDays() < 30) {
            System.out.println("员工:" + leaveRequest.getEmpName() + "==请假天数:" + leaveRequest.getLeaveDays() + "==请假原因:" + leaveRequest.getReason());
            System.out.println("总经理:" + this.name + "审批通过!");
        } else {
            System.out.println("结束：" + leaveRequest.getLeaveDays());
        }
    }
}
