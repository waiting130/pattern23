package com.chenyu;

/**
 * 经理
 *
 * @author yu_chen
 * @create 2018-01-25 10:38
 **/
public class Manager extends Leader {

    public Manager(String name) {
        super(name);
    }

    @Override
    public void handlerRequest(LeaveRequest leaveRequest) {
        if (leaveRequest.getLeaveDays() < 10) {
            System.out.println("员工:" + leaveRequest.getEmpName() + "==请假天数:" + leaveRequest.getLeaveDays() + "==请假原因:" + leaveRequest.getReason());
            System.out.println("经理:" + this.name + "审批通过!");
        } else {
            if (this.nextLeader != null) {
                this.nextLeader.handlerRequest(leaveRequest);
            }
        }
    }
}
