package com.chenyu;

public interface Strategy {
	public double getPrice(double standardPrice);
}
