package com.chenyu.my;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yu_chen
 * @create 2018-01-25 15:32
 **/
public class Subject {

    private List<Observer> observerList = new ArrayList<>();


    /**
     * 注册观察者
     * @param observer
     */
    public void registerObserver(Observer observer) {
        observerList.add(observer);
    }

    /**
     * 移除观察者
     * @param observer
     */
    public void removeObserver(Observer observer) {
        observerList.remove(observer);
    }

    /**
     * 通知所有观察者更新
     */
    public void notifyAllObserverUpdate() {
        for (Observer observer : observerList) {
            observer.update(this);
        }
    }
}
