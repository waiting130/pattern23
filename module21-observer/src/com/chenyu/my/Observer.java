package com.chenyu.my;

/**
 * 观察者
 *
 * @author
 * @create 2018-01-25 15:32
 **/
public interface Observer {

    /**
     * 更新状态
     * @param subject
     */
    void update(Subject subject);
}
