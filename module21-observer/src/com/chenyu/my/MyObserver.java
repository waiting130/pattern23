package com.chenyu.my;

/**
 * 具体观察者
 *
 * @author yu_chen
 * @create 2018-01-25 15:43
 **/
public class MyObserver implements Observer {

    private int state;

    public int getState() {
        return state;
    }

    @Override
    public void update(Subject subject) {
        this.state = ((ConcreteSubject) subject).getState();
    }
}
