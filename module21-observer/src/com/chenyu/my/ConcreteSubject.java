package com.chenyu.my;

/**
 * 具体的目标对象
 *
 * @author yu_chen
 * @create 2018-01-25 15:37
 **/
public class ConcreteSubject extends Subject {

    private int state;

    /**
     * 当状态更新时通知所有观察者去更新自己的状态
     *
     * @param state
     */
    public void setState(int state) {
        this.state = state;
        this.notifyAllObserverUpdate();
    }

    public int getState() {
        return state;
    }
}
