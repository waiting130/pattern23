package com.chenyu.my;

/**
 * @author yu_chen
 * @create 2018-01-25 16:03
 **/
public class Client {

    public static void main(String[] args) {
        ConcreteSubject subject = new ConcreteSubject();

        MyObserver observer1 = new MyObserver();
        MyObserver observer2 = new MyObserver();
        MyObserver observer3 = new MyObserver();

        subject.registerObserver(observer1);
        subject.registerObserver(observer2);
        subject.registerObserver(observer3);


        subject.setState(100);

        System.out.println(observer1.getState());
        System.out.println(observer2.getState());
        System.out.println(observer3.getState());
    }
}
