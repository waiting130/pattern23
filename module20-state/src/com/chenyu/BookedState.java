package com.chenyu;

/**
 * 已预订状态
 *
 * @author yu_chen
 * @create 2018-01-25 15:15
 **/
public class BookedState implements State {

    @Override
    public void handle() {
        System.out.println("已预订状态");
    }
}
