package com.chenyu;

/**
 * @author yu_chen
 * @create 2018-01-25 15:19
 **/
public class Client {

    public static void main(String[] args) {
        Context ctx = new Context();

        ctx.setState(new FreeState());
        ctx.setState(new BookedState());
    }
}
