package com.chenyu;

/**
 * 环境
 *
 * @author yu_chen
 * @create 2018-01-25 15:17
 **/
public class Context {

    //如果是银行系统，ctx就是账号

    private State state;

    public void setState(State state) {
        System.out.println("修改状态");
        this.state = state;
        state.handle();
    }


}
