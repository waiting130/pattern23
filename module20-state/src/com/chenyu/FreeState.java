package com.chenyu;

/**
 * @author yu_chen
 * @create 2018-01-25 15:16
 **/
public class FreeState implements State{

    @Override
    public void handle() {
        System.out.println("空闲状态，可以预订");
    }
}
