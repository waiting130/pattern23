package com.chenyu;

/**
 * 状态类接口
 *
 * @author
 * @create 2018-01-25 15:14
 **/
public interface State {

    /**
     * 处理
     */
    void handle();
}
