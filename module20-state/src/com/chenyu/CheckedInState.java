package com.chenyu;

/**
 * 已入住状态
 *
 * @author yu_chen
 * @create 2018-01-25 15:16
 **/
public class CheckedInState implements State {

    @Override
    public void handle() {
        System.out.println("已入住");
    }
}
