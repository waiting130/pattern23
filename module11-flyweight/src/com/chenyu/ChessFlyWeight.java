package com.chenyu;

/**
 * 棋子类
 * 享元类
 *
 * @author yu_chen
 * @create 2018-01-25 9:51
 **/
public interface ChessFlyWeight {

    /**
     * 设置颜色
     *
     * @param color
     */
    default void setColor(String color) {

    }


    /**
     * 得到棋子的颜色
     *
     * @return color
     */
    String getColor();

    /**
     * 坐标展示
     *
     * @param coordinate
     */
    void display(Coordinate coordinate);
}
