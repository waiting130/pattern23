package com.chenyu;

import java.util.HashMap;
import java.util.Map;

/**
 * 享元工厂类
 *
 * @author yu_chen
 * @create 2018-01-25 9:55
 **/
public class ChessFlyWeightFactory {

    private static Map<String, ChessFlyWeight> chessFlyWeightMap = new HashMap<>();

    /**
     * 根据颜色得到享元的实例
     *
     * @param color
     * @return
     */
    public static ChessFlyWeight getChessFlyWeightInstance(String color) {
        ChessFlyWeight chessFlyWeight = chessFlyWeightMap.get(color);
        if (chessFlyWeight == null) {
            chessFlyWeight = new ConcreteChess(color);
            chessFlyWeightMap.put(color, chessFlyWeight);
        }
        return chessFlyWeight;
    }
}
