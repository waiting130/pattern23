package com.chenyu;

/**
 * @author yu_chen
 * @create 2018-01-25 10:05
 **/
public class Client {

    public static void main(String[] args) {
        ChessFlyWeight chess1 = ChessFlyWeightFactory.getChessFlyWeightInstance("黑色");
        ChessFlyWeight chess2 = ChessFlyWeightFactory.getChessFlyWeightInstance("黑色");
        ChessFlyWeight chess3 = ChessFlyWeightFactory.getChessFlyWeightInstance("白色");

        System.out.println(chess1);
        System.out.println(chess2);
        System.out.println(chess3);

        chess1.display(new Coordinate(10, 10));
        chess2.display(new Coordinate(20, 20));

        System.out.println(chess1);
        System.out.println(chess2);
    }
}
