package com.chenyu;

/**
 * @author yu_chen
 * @create 2018-01-25 9:58
 **/
public class ConcreteChess implements ChessFlyWeight {


    private String color;

    public ConcreteChess(String color) {
        this.color = color;
    }

    @Override
    public String getColor() {
        return color;
    }

    @Override
    public void display(Coordinate coordinate) {
        System.out.println("棋子颜色=" + color);
        System.out.println("棋子位置x=" + coordinate.getX() + ",y=" + coordinate.getY());
    }
}
